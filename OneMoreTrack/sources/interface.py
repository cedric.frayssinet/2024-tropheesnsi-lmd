# -*- coding: utf-8 -*-

#Importation
from customtkinter import *
from tkinter import messagebox
from PIL import Image
import webbrowser
import sqlite3
from pyvis.network import Network
import networkx as nx
from random import randint
import community.community_louvain
import os
import csv
import urllib.request

dico = {}

with open('dico_adjacence.txt', 'r') as f:
    dico = eval(f.read().strip())

#Base
app = CTk()
app.title('One More Track')
app.geometry("800x600")

theme = []

def recherche():

	connexion_BD = sqlite3.connect('albums.db')
	c = connexion_BD.cursor()
	
	search = entree.get()
	entree.delete(0,'end')
	result = ''
	parametre = 1
	
	if search != "":
		c.execute(f"SELECT id, title, artist, year, tracklist, label, genre, style, country, cover_image FROM albums WHERE title LIKE '%{search}%'")
		
		liste_data = c.fetchall()
		
		if liste_data == []:
			c.execute(f"SELECT id, title, artist, year, tracklist, label, genre, style, country, cover_image FROM albums WHERE artist LIKE '%{search}%'")
			liste_data = c.fetchall()
			parametre = 2
		
		if liste_data != []:
			indices = []
			for res in liste_data:
				indice = len(search)/len(res[parametre])
				indices.append(indice)
			
			result = liste_data[indices.index(max(indices))]
			
			if not os.path.isfile(f"cache/{result[0]}.jpeg"):
				urllib.request.urlretrieve(result[-1], f"cache/{result[0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{result[0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(300, 300))
			album.configure(image=img, text="")
			
			label_res.configure(text=f"Titre: {result[1]}\nArtiste: {result[2]}"+2*"\n"+"-"+"\n-".join(eval(result[4])))
			reco_texte.configure(text="Albums pouvant vous plaire:")
		
			reco = []

			with open('albums.csv', 'r', encoding='utf8') as data:
				reader = csv.DictReader(data, delimiter=';')
			
				for d in list(reader):
					if int(d['id']) != result[0]:
						score = 0
						if result[2] == d['artist']:
							score += 5
						
						score += (40-abs(int(res[3])-int(d['year'])))/2
						score += sum(check in eval(result[5]) for check in eval(d['label']))*2						
						score += sum(check in eval(result[6]) for check in eval(d['genre']))*18
						score += sum(check in eval(result[7]) for check in eval(d['style']))*10
						
						if result[8] == d['country']:
							score += 6
						
						score += randint(0, 10)
						
						reco.append((d['id'], d['title'], d['artist'], d['cover_image'], score))
			
			reco.sort(key=lambda x: x[-1], reverse=True)
			
			
			
			if not os.path.isfile(f"cache/{reco[0][0]}.jpeg"):
				urllib.request.urlretrieve(reco[0][3], f"cache/{reco[0][0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{reco[0][0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_1_label.configure(image=img, text="\n"*10+f"{reco[0][1]}\n{reco[0][2]}")
			
			if not os.path.isfile(f"cache/{reco[1][0]}.jpeg"):
				urllib.request.urlretrieve(reco[1][3], f"cache/{reco[1][0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{reco[1][0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_2_label.configure(image=img, text="\n"*10+f"{reco[1][1]}\n{reco[1][2]}")
			
			if not os.path.isfile(f"cache/{reco[2][0]}.jpeg"):
				urllib.request.urlretrieve(reco[2][3], f"cache/{reco[2][0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{reco[2][0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_3_label.configure(image=img, text="\n"*10+f"{reco[2][1]}\n{reco[2][2]}")
			
			if not os.path.isfile(f"cache/{reco[3][0]}.jpeg"):
				urllib.request.urlretrieve(reco[3][3], f"cache/{reco[3][0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{reco[3][0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_4_label.configure(image=img, text="\n"*10+f"{reco[3][1]}\n{reco[3][2]}")
			
			if not os.path.isfile(f"cache/{reco[4][0]}.jpeg"):
				urllib.request.urlretrieve(reco[4][3], f"cache/{reco[4][0]}.jpeg")
			
			album_img = Image.open('cache/'+f"{reco[4][0]}.jpeg")
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_5_label.configure(image=img, text="\n"*10+f"{reco[4][1]}\n{reco[4][2]}")
			
			
		
		else:
			
			album_img = Image.open('images/album_bg.png')
			img = CTkImage(dark_image=album_img, light_image=album_img, size=(300, 300))
			album.configure(image=img, text="Aucun Résultat")
			label_res.configure(text="")
			reco_texte.configure(text="")
			
			fond_img = Image.open('images/album_bg.png')
			fond = CTkImage(dark_image=album_img, light_image=album_img, size=(100, 100))
			reco_1_label.configure(image=fond, text="")
			reco_2_label.configure(image=fond, text="")
			reco_3_label.configure(image=fond, text="")
			reco_4_label.configure(image=fond, text="")
			reco_5_label.configure(image=fond, text="")



def entrer(event):
	recherche()    


    
def graph():
	
	if theme == []:
		messagebox.showwarning('Attention','Aucun theme de musique sélectionné.')
	
	else:
		if len(theme) < 3 or messagebox.askokcancel('Attention', f'Vous avez sélectionné {len(theme)} themes, votre ordinateur pourrait ralentir\nVoulez vous continuer ?'):
			net = Network(notebook = True, bgcolor="#111111", font_color='white')
			nx.width = '100%'
			nx.height = '100%'
			net.repulsion()
			
			G = nx.Graph()
			
			with open('albums.csv', 'r', encoding='utf8') as f:
				reader = csv.DictReader(f, delimiter=';')
				data = list(reader)
				data_f = [i for i in data if any(check in eval(i['genre']) for check in theme)]  
				
				for i in data_f:
					name = f'{i["title"]}\n{i["artist"]}'
					
					if i['cover_image'] != '':
						G.add_node(name, shape='image', image = i['cover_image'])
						# G.add_node(name)
					else:
						G.add_node(name)

					for album_id in dico[f'{i["id"]}']:
						for k in data_f:
							if k['id'] == album_id and not G.has_edge(f'{k["title"]}\n{k["artist"]}', name):
								G.add_edge(name, f'{k["title"]}\n{k["artist"]}')

					# ~ for j in range(i):
						# ~ if not G.has_edge(name, f'{data[j]["title"]}\n{data[j]["artist"]}'):
			
							# ~ if data_f[i]['artist'] == data_f[j]['artist']:
								# ~ G.add_edge(name, f'{data_f[j]["title"]}\n{data_f[j]["artist"]}')
							# ~ elif any(check in [data_f[i]['artist']]+eval(data_f[i]['members_name'])+eval(data_f[i]['featuring']) for check in [data_f[j]['artist']]+eval(data_f[j]['members_name'])+eval(data_f[j]['featuring'])): 
								# ~ G.add_edge(name, f'{data_f[j]["title"]}\n{data_f[j]["artist"]}')
			
			nx.set_node_attributes(G, 8, 'size')
			
			
			communities = community.community_louvain.best_partition(G)
			nx.set_node_attributes(G, communities, 'group')


		#     print(nx.average_shortest_path_length(G))
		#     print(nx.shortest_path(G, 'Demon Days\nGorillaz', 'Head Hunters\nHerbie Hancock'))
			
			
			net.from_nx(G)
			
			nom = 'musique'
			net.show(f"{nom}.html")
			
			content = ""
			with open(f"{nom}.html", 'r') as p:
				content = p.read().replace("width: 100%", "width: 100%; height: 98%")
			with open(f"{nom}.html", 'w') as p:
				p.write(content)
			
			path = os.path.abspath(f"{nom}.html")
			webbrowser.open(f"file://{path}")

def vider_cache():
	for f in os.listdir('cache/'):
		os.remove('cache/'+f)
	messagebox.showinfo('Information','Le cache a bien été vidé')



def rock():
	if "Rock" in theme:
		theme.remove("Rock")
	else:
		theme.append("Rock")

def jazz():
	if "Jazz" in theme:
		theme.remove("Jazz")
	else:
		theme.append("Jazz")


def hiphop():
	if "Hip Hop" in theme:
		theme.remove("Hip Hop")
	else:
		theme.append("Hip Hop")

def pop_genre():
	if "Pop" in theme:
		theme.remove("Pop")
	else:
		theme.append("Pop")

def soul():
	if "Funk / Soul" in theme:
		theme.remove("Funk / Soul")
	else:
		theme.append("Funk / Soul")


def reggae():
	if "Reggae" in theme:
		theme.remove("Reggae")
	else:
		theme.append("Reggae")

	
def blues():
	if "Blues" in theme:
		theme.remove("Blues")
	else:
		theme.append("Blues")
		
def electronic():
	if "Electronic" in theme:
		theme.remove("Electronic")
	else:
		theme.append("Electronic")
		


#Le theme de l'interface
set_appearance_mode("dark")


# Création de la frame
titre_frame = CTkFrame(app, fg_color="#242424")
titre_frame.pack(side='top', fill='x')

graph_frame= CTkFrame(app, fg_color="#242424", corner_radius=0)
graph_frame.pack(side='left', fill='y')

theme_frame = CTkScrollableFrame(graph_frame, fg_color="#242424", 
								 corner_radius=0, orientation="vertical", scrollbar_button_color="#61727C")
theme_frame.pack(fill='y', side='top', expand=True)

recherche_frame = CTkFrame(app, fg_color="#242424", corner_radius=0)
recherche_frame.pack(side="bottom", fill='x')


# Chargement de l'image
loupe = Image.open("images/loupe.png")

titre = CTkLabel(titre_frame, text="One More Track")
titre.pack(side='left', expand=True, fill='x')

# Paramètre de la zone de saisie
entree = CTkEntry(master=recherche_frame, placeholder_text="Rechercher", text_color="#61727C")
entree.pack(side="left", expand=True, fill="x", padx=(10, 0), pady=10)

# Paramètre du bouton de recherche
recherche_btn = CTkButton(master=recherche_frame, text="", corner_radius=32, command=recherche, 
				fg_color="#317386", hover_color="#637E8D", image=(CTkImage(dark_image=loupe, light_image=loupe)))
recherche_btn.pack(side="left", padx=(0, 10), pady=10)

clear_btn = CTkButton(master=recherche_frame, text="Vider le cache", corner_radius=32, command=vider_cache, 
				fg_color="#317386", hover_color="#637E8D", width=40)
clear_btn.pack(side="left", padx=(0, 10), pady=10)

#Parametre graphe
graph = CTkButton(graph_frame, text="Générer Graphe", corner_radius=32, command=graph,
				fg_color="#317386", hover_color="#637E8D")
graph.pack(side="bottom", pady=(0,10))


results_frame = CTkFrame(app, fg_color="#242424", corner_radius=0)
results_frame.pack(fill=BOTH, expand=1)

album_img = Image.open('images/album_bg.png')
img = CTkImage(dark_image=album_img, light_image=album_img, size=(300, 300))

album=CTkLabel(results_frame, image=img, text="Veuillez rechercher un album")
album.pack(side='left', fill=BOTH, expand=True)

label_res = CTkLabel(results_frame, text="", justify='left', anchor='w')
label_res.pack(side='right', fill=BOTH, expand=True)


recom_frame = CTkFrame(app, fg_color="#242424", corner_radius=0)
recom_frame.pack(fill=BOTH, expand=True, side='right')

reco_texte= CTkLabel(recom_frame, text="")
reco_texte.pack(fill=BOTH, side = 'top')

reco_1_label = CTkLabel(recom_frame, text="")
reco_1_label.pack(side='left', padx=10)

reco_2_label = CTkLabel(recom_frame, text="")
reco_2_label.pack(side='left', padx=10)

reco_3_label = CTkLabel(recom_frame, text="")
reco_3_label.pack(side='left', padx=10)

reco_4_label = CTkLabel(recom_frame, text="")
reco_4_label.pack(side='left', padx=10)

reco_5_label = CTkLabel(recom_frame, text="")
reco_5_label.pack(side='left', padx=10)


#Liste de styles de musique
Rock = CTkCheckBox(theme_frame, text="Rock", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=rock)
Rock.pack(side="top", pady=(10,10))

Jazz = CTkCheckBox(theme_frame, text="Jazz", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=jazz)
Jazz.pack(side="top", pady=(10,10))

Hip_Hop = CTkCheckBox(theme_frame, text="Hip-Hop", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=hiphop)
Hip_Hop.pack(side="top", pady=(10,10))

Pop = CTkCheckBox(theme_frame, text="Pop", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=pop_genre)
Pop.pack(side="top", pady=(10,10))

Soul = CTkCheckBox(theme_frame, text="Funk / Soul", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=soul)
Soul.pack(side="top", pady=(10,10))

Reggae = CTkCheckBox(theme_frame, text="Reggae", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=reggae)
Reggae.pack(side="top", pady=(10,10))

Blues = CTkCheckBox(theme_frame, text="Blues", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=blues)
Blues.pack(side="top", pady=(10,10))

Electronic = CTkCheckBox(theme_frame, text="Electronic", fg_color='#317A86', checkbox_height=30,
				  checkbox_width=30, corner_radius=36, command=electronic)
Electronic.pack(side="top", pady=(10,10))

app.bind('<Return>', entrer)

app.mainloop()
